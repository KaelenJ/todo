"use strict";
/*Helpers carry data from a template to whatever the key value is thus meaning in this case the name value is todolist: and the keyvalues is the array*/
Template.todos.helpers({
    todoList: function () {
        return Collections.Todo.find({});
    }
});

